"use strict";

jQuery(function ($) {
  $(".phone").inputmask({
    mask: "+7(999)-999-99-99",
    showMaskOnHover: false
  });
  $(".g-header__mob").on("click", function (e) {
    e.preventDefault();
    $(".g-header__what").slideToggle("fast");
    $(".g-header__info").slideToggle("fast");
    $(".g-header__button").slideToggle("fast");
    $(".g-nav").slideToggle("fast");
  });
  $(".open-modal").on("click", function (e) {
    e.preventDefault();
    $(".g-modal").toggle();
  });
  $(".g-modal__centered").on("click", function (e) {
    e.preventDefault();

    if (e.target.className === "g-modal__centered") {
      $(".g-modal").hide();
    }
  });
  $(".g-modal__close").on("click", function (e) {
    e.preventDefault();
    $(".g-modal").hide();
  });
  $(".g-tabs__header li").click(function () {
    var tab_id = $(this).attr("data-tab");
    $(".g-tabs__header li").removeClass("current");
    $(".g-tabs__content").removeClass("current");
    $(this).addClass("current");
    $("#" + tab_id).addClass("current");
  });
  $(".g-catalog__sort").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("g-catalog__sort_active");
    $(this).next().slideToggle("fast");
  });
  new Swiper(".g-top", {
    pagination: {
      el: ".g-top .swiper-pagination",
      clickable: true
    },
    navigation: {
      nextEl: ".g-top .swiper-button-next",
      prevEl: ".g-top .swiper-button-prev"
    },
    loop: true
  });
  new Swiper(".g-home-fix__cards", {
    scrollbar: {
      el: ".g-home-fix .swiper-scrollbar"
    },
    navigation: {
      nextEl: ".g-home-fix .swiper-button-next",
      prevEl: ".g-home-fix .swiper-button-prev"
    },
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
  new Swiper(".g-home-reviews__cards", {
    navigation: {
      nextEl: ".g-home-reviews .swiper-button-next",
      prevEl: ".g-home-reviews .swiper-button-prev"
    },
    loop: true
  });
  new Swiper(".g-papers__cards", {
    navigation: {
      nextEl: ".g-papers .swiper-button-next",
      prevEl: ".g-papers .swiper-button-prev"
    },
    loop: true
  });
  /* Delete */

  /* Delete */

  /* Delete */

  /* Delete */

  /* Delete */

  new Swiper(".s-home-projects__cards", {
    navigation: {
      nextEl: ".s-home-projects .swiper-button-next",
      prevEl: ".s-home-projects .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper(".s-home-news__cards", {
    navigation: {
      nextEl: ".s-home-news .swiper-button-next",
      prevEl: ".s-home-news .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      768: {
        slidesPerView: 2
      },
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper(".s-papers__cards", {
    navigation: {
      nextEl: ".s-papers .swiper-button-next",
      prevEl: ".s-papers .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 4
      },
      1200: {
        slidesPerView: 6
      }
    }
  });
  new Swiper("#smi .s-smi__cards", {
    navigation: {
      nextEl: "#smi .swiper-button-next",
      prevEl: "#smi .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper("#gallery .s-smi__cards", {
    navigation: {
      nextEl: "#gallery .swiper-button-next",
      prevEl: "#gallery .swiper-button-prev"
    },
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  var galleryThumbs = new Swiper(".s-history__thumbs", {
    spaceBetween: 20,
    slidesPerView: 1,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    allowTouchMove: false
  });
  var galleryTop = new Swiper(".s-history__top", {
    spaceBetween: 20,
    thumbs: {
      swiper: galleryThumbs
    },
    navigation: {
      nextEl: ".s-history .swiper-button-next",
      prevEl: ".s-history .swiper-button-prev"
    }
  });
  var galleryThumbs2 = new Swiper(".gallery-thumbs", {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true
  });
  var galleryTop2 = new Swiper(".gallery-top", {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs2
    }
  });
});
//# sourceMappingURL=main.js.map